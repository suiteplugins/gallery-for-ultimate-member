# Copyright (C) 2017 
# This file is distributed under the same license as the  package.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: "
"https://wordpress.org/support/plugin//Users/kaz/localdev/www/ultimate_"
"member/htdocs/wp-content/plugins/gallery-for-ultimate-member/um-gallery\n"
"POT-Creation-Date: 2017-08-14 03:55:02+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n1.0.0\n"

#: admin/templates/gallery-list.php:9
#: includes/class-um-gallery-lite-template.php:149
msgid "Albums"
msgstr ""

#: admin/templates/gallery-list.php:48 templates/um-gallery/albums.php:31
msgid "No albums found"
msgstr ""

#: admin/templates/gallery-view.php:3
msgid "No album selected. Go back and try again"
msgstr ""

#: admin/templates/gallery-view.php:12
msgid "Album"
msgstr ""

#: admin/templates/gallery-view.php:35
msgid "No photos found"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:15
#: includes/class-um-gallery-lite-admin.php:16
msgid "UM Gallery"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:23
#: includes/class-um-gallery-lite-admin.php:24
msgid "Settings"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:68
msgid "Role select"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:69
msgid "Roles Permission"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:70
msgid "Which user roles are allowed to upload photos"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:71
msgid "Give album creation access to certain roles. Leave blank to allow it to all"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:78
msgid "Show on Main Tab"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:80
msgid "If enabled, recent photo uploads will be placed on a user's profile main tab"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:81
msgid "Yes"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:82
msgid "No"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:88
msgid "Photos on profile"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:89
msgid "Set the number of photos on profile"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:95
msgid "Pro Version"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:96
msgid "Looking for more?"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:98
msgid ""
"Take a look at <a href=\"%s\" target=\"_blank\">UM Gallery Pro</a><div><a "
"href=\"%s\" target=\"_blank\"><img src=\"%s\" border=\"0\" style=\"width: "
"212px !important;\" /></a></div>"
msgstr ""

#: includes/class-um-gallery-lite-admin.php:193
#: includes/class-um-gallery-lite-template.php:60
msgid "Gallery"
msgstr ""

#: includes/class-um-gallery-lite-ajax.php:66
msgid "Untitled Album"
msgstr ""

#: includes/class-um-gallery-lite-template.php:103
msgid "Add Photo"
msgstr ""

#: includes/class-um-gallery-lite-template.php:120
msgid "Back to Albums"
msgstr ""

#: includes/class-um-gallery-lite-template.php:151
msgid "Add Album"
msgstr ""

#: includes/um-gallery-functions.php:132
msgid "%s photo"
msgid_plural "%s photos"
msgstr[0] ""
msgstr[1] ""

#: templates/um-gallery/manage/album-form.php:7
msgid "Manage Album"
msgstr ""

#: templates/um-gallery/manage/album-form.php:12
#: templates/um-gallery/manage/album-form.php:13
msgid "Enter Album Name"
msgstr ""

#: templates/um-gallery/manage/album-form.php:28 um-gallery.php:177
msgid "Save"
msgstr ""

#: templates/um-gallery/manage/album-form.php:29 um-gallery.php:179
msgid "Cancel"
msgstr ""

#: um-gallery.php:178
msgid "<i class=\"um-faicon-pencil\"></i> Edit Caption"
msgstr ""

#: um-gallery.php:182
msgid "Upload your photos"
msgstr ""

#: um-gallery.php:183
msgid "Upload Complete"
msgstr ""

#: um-gallery.php:184
msgid "No photos found."
msgstr ""