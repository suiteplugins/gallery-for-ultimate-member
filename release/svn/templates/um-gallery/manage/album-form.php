<?php
global $album;
$album_id = ! empty( $album ) ? (int) $album->id : 0;
?>
<div class="um-gallery-form-wrapper" id="um-gallery-album">
  <div class="um-modal-header">
    <?php _e('Manage Album'); ?>
  </div>
  <div class="um-modal-body">
    <form>
      <div class="um-gallery-form-header">
          <input type="hidden" name="album_name" id="album_name" placeholder="<?php _e( 'Enter Album Name', 'gallery-for-ultimate-member' ); ?>" value="<?php echo ! empty( $album ) ? esc_attr( $album->album_name ) : ''; ?>" />
          <input type="hidden" name="album_description" id="album_description" placeholder="<?php _e( 'Enter Album Name', 'gallery-for-ultimate-member' ); ?>" value="<?php echo ! empty( $album ) ? esc_attr( $album->album_description ) : ''; ?>" />
      </div>
      <div >
        <div class="um-clear"></div>
      </div>
      <div id="dropzone" class="dropzone um-gallery-upload"> </div>
      <input type="hidden" name="album_id" value="<?php echo absint( $album_id ); ?>" />
    </form>
    <div class="um-modal-footer">
      <div class="um-modal-left">
        <div class="um-gallery-form-field">
          <input type="hidden" name="album_privacy" id="album_privacy" value="public" />
        </div>
      </div>
      <div class="um-modal-right"> <a href="#" class="um-modal-btn image" id="um-gallery-save" data-id="<?php echo absint( $album_id ); ?>" data-type="album">
        <?php _e('Save', 'gallery-for-ultimate-member'); ?>
        </a> <a href="#" class="um-modal-btn um-gallery-close alt" id="um-gallery-cancel">  <?php _e('Cancel', 'gallery-for-ultimate-member'); ?></a> </div>
      <div class="um-clear"></div>
    </div>
  </div>
</div>
